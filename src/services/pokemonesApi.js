import axios from "axios"

const urlApi = 'https://pokeapi.co/api/v2/pokemon/'

//https://pokeapi.co/api/v2/pokemon/?offset=0&limit=8
const consultaPokemones = (params) => {
    const pokemonApi = axios.create({
        baseURL: urlApi,
        params: {
            offset: params.offset,
            limit: params.limit,
        }
    })
    
    return pokemonApi.get()
}
//https://pokeapi.co/api/v2/pokemon/ditto
const consultaPokemon = (nombre) => {
    const pokemonApi = axios.create({
        baseURL: urlApi
    })
    
    return pokemonApi.get('/'+nombre)
}

export {
    consultaPokemon,
    consultaPokemones
}