import { createStore } from 'vuex'
import modulo from "@/store/modulo";

export default createStore({
  modules: {
    modulo
  }
})
