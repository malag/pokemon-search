import { consultaPokemon, consultaPokemones } from "@/services/pokemonesApi"

const devuelvePokemones = async (context, params) => {

    console.log('devuelvePokemones')
    const pokemonesApi = await consultaPokemones (params)
    const { results } = pokemonesApi.data
    // console.log(pokemonesApi.data)
    context.commit('devuelvePokemones', results)
}

const devuelvePokemon = async (context, nombre) => {
    
    console.log('Buscando a: ', nombre)
    const pokemonApi = await consultaPokemon(nombre)
    const poke = pokemonApi.data
    console.log('devuelvePokemon')
    context.commit('devuelvePokemon', poke)    
}

export {
    devuelvePokemones,
    devuelvePokemon
}