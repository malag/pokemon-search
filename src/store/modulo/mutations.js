import state from "./state"

const devuelvePokemones = (state, pokemones) => {
    state.pokemones = pokemones
}
const devuelvePokemon = (state, pokemon) => {
    state.pokemon = pokemon
}

export {
    devuelvePokemones,
    devuelvePokemon
}